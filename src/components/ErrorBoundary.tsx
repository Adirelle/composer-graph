import { Component, PropsWithChildren } from "react";

export type ErrorBoundaryState =
  | { hasError: false }
  | { hasError: true; error: Error };

export interface ErrorBoundaryProps {
  label?: string;
}

export default class ErrorBoundary extends Component<
  PropsWithChildren<ErrorBoundaryProps>,
  ErrorBoundaryState
> {
  constructor(props: PropsWithChildren<ErrorBoundaryProps>) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error: Error): ErrorBoundaryState {
    return { hasError: true, error };
  }

  render() {
    if (this.state.hasError) {
      const { name, message } = this.state.error;

      return (
        <div>
          <h3>{name}</h3>
          <code>
            <pre>{message}</pre>
          </code>
          <button onClick={() => this.setState({ hasError: false })}>
            Retry
          </button>
        </div>
      );
    }

    return this.props.children;
  }
}
