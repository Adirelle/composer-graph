import { Project } from "../../model";

export interface ProjectHeaderProps {
  project: Project;
}

export default function ProjectHeader({
  project: { root },
}: ProjectHeaderProps) {
  return (
    <div>
      <h3>
        {root.name} {root.version}
      </h3>
    </div>
  );
}
