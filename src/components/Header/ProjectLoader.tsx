import { useState } from "react";
import { Project } from "../../model";

export interface ProjectLoaderProps {
  setProject(project: Project): void;
}

async function parseJSONFile<T>(file: File): Promise<T> {
  try {
    return JSON.parse(await file.text());
  } catch (e) {
    throw new Error(`error in ${file.name}: ${e}`);
  }
}

async function loadProject(files: FileList): Promise<Project> {
  let rootFile: File | undefined;
  let lockFile: File | undefined;
  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    switch (file.name) {
      case "composer.json":
        rootFile = file;
        break;
      case "composer.lock":
        lockFile = file;
        break;
    }
  }

  if (!rootFile) {
    throw new Error("missing composer.json");
  }

  return new Project(
    await parseJSONFile(rootFile),
    lockFile ? await parseJSONFile(lockFile) : undefined
  );
}

export default function ProjectLoader({ setProject }: ProjectLoaderProps) {
  const [feedback, setFeedback] = useState("");

  return (
    <div>
      <div>composer.json & composer.lock:</div>
      <input
        type="file"
        multiple
        accept="composer.lock,composer.json,.lock,.json,application/json"
        onInput={async (ev) => {
          const files = ev.currentTarget.files;
          if (files) {
            try {
              setProject(await loadProject(files));
              setFeedback("");
            } catch (e) {
              setFeedback(`Error: ${e}`);
            }
          }
        }}
      />
      {feedback}
    </div>
  );
}
