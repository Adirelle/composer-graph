import { PropsWithChildren } from "react";

export interface CheckBoxProps {
  checked: boolean;
  set: (checked: boolean) => void;
}

export default function CheckBox({
  checked,
  set,
  children,
}: PropsWithChildren<CheckBoxProps>) {
  return (
    <div>
      <input
        type="checkbox"
        checked={checked}
        onChange={(ev) =>
          checked !== ev.target.checked && set(ev.target.checked)
        }
      />
      {children}
    </div>
  );
}
