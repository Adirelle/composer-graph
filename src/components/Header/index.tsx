import { Project } from "../../model";
import BuildSettings, { BuildSettingsProps } from "./BuildSettings";
import ProjectHeader from "./ProjectHeader";
import ProjectLoader, { ProjectLoaderProps } from "./ProjectLoader";

export interface HeaderProps extends ProjectLoaderProps, BuildSettingsProps {
  project: Project | undefined;
}

export default function Header({
  project,
  setProject,
  options,
  setOptions,
}: HeaderProps) {
  return (
    <>
      <div id="project">
        {project ? (
          <ProjectHeader project={project} />
        ) : (
          <ProjectLoader setProject={setProject} />
        )}
      </div>

      <div id="options">
        <BuildSettings options={options} setOptions={setOptions} />
      </div>
    </>
  );
}
