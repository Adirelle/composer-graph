import { PropsWithChildren } from "react";
import { FilterOptions } from "../../model";
import CheckBox from "./CheckBox";

export interface BuildSettingsProps {
  options: FilterOptions;
  setOptions(options: FilterOptions): void;
}

interface OptionCheckBoxProps extends BuildSettingsProps {
  option: keyof FilterOptions;
}

function OptionCheckBox({
  option,
  options,
  setOptions,
  children,
}: PropsWithChildren<OptionCheckBoxProps>) {
  const checked = !options[option];
  const set = (newChecked: boolean) => {
    if (checked !== newChecked) {
      setOptions({ ...options, [option]: !newChecked });
    }
  };

  return (
    <CheckBox checked={checked} set={set}>
      {children}
    </CheckBox>
  );
}

export default function BuildSettings(props: BuildSettingsProps) {
  return (
    <section
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-evenly",
      }}
    >
      <OptionCheckBox option="noIndirect" {...props}>
        Indirect dependencies
      </OptionCheckBox>
      <OptionCheckBox option="noDev" {...props}>
        Development dependencies
      </OptionCheckBox>
      <OptionCheckBox option="noNamespaces" {...props}>
        Namespaces
      </OptionCheckBox>
      <OptionCheckBox option="noPlatform" {...props}>
        Platform packages
      </OptionCheckBox>
    </section>
  );
}
