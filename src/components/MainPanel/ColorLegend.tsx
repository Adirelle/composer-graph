import { ColorScheme } from "../../viewmodel";
import ColorWatch from "./ColorWatch";

export interface ColorLegendProps {
  scheme: ColorScheme;
}

export default function ColorLegend({ scheme }: ColorLegendProps) {
  return (
    <div>
      {Array.from(scheme.colors()).map(({ color, label }, idx) => (
        <div key={idx}>
          <ColorWatch color={color} />
          {label}
        </div>
      ))}
    </div>
  );
}
