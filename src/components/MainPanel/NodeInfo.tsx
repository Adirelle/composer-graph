import { Node } from "../../model";

export interface NodeInfoProps {
  node: Node;
}

export default function NodeInfo({
  node: { id, version, type },
}: NodeInfoProps) {
  return (
    <div>
      <div>
        {type}: {id}
      </div>
      <div>Version: {version}</div>
    </div>
  );
}
