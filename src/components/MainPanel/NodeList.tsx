import { Graph, Node } from "../../model";
import ColorWatch from "./ColorWatch";
import "./NodeList.css";

interface NodeListItemProps {
  node: Node;
  color: string;
  onHover: (id: string | null) => void;
}

function NodeListItem({ node: { id }, color, onHover }: NodeListItemProps) {
  return (
    <li onMouseEnter={() => onHover(id)} onMouseLeave={() => onHover(null)}>
      <ColorWatch color={color} /> {id}
    </li>
  );
}

export interface NodeListProps {
  graph: Graph;
  color: (node: Node) => string;
  onHover: (id: string | null) => void;
}

export default function NodeList({ graph, color, onHover }: NodeListProps) {
  return (
    <>
      <div>{graph.size} Packages</div>
      <ul id="nodeList">
        {Array.from(graph.nodes())
          .sort((a, b) => (a.id < b.id ? -1 : 1))
          .map((node) => (
            <NodeListItem
              key={node.id}
              node={node}
              color={color(node)}
              onHover={onHover}
            />
          ))}
      </ul>
    </>
  );
}
