import { useCallback, useMemo, useState } from "react";
import { LinkObject, NodeObject } from "react-force-graph-2d";
import { useResizeDetector } from "react-resize-detector";
import { Builder, FilterOptions, filterVisitor, Project } from "../../model";
import {
  ColorMap,
  ConstantColor,
  createDepthColorMap,
  isColorScheme,
  PredicateColorMap,
  toNodeID,
} from "../../viewmodel";
import { NeverPredicate, Predicate } from "../../viewmodel/predicate";
import { StabilityMap } from "../../viewmodel/stability";
import ErrorBoundary from "../ErrorBoundary";
import ColorLegend from "./ColorLegend";
import GraphPanel from "./GraphPanel";
import NodeInfo from "./NodeInfo";
import NodeList from "./NodeList";

export interface MainPanelProps {
  project: Project;
  options: FilterOptions;
}

//const defaultNodeColor = new ConstantColor<NodeObject>("#47C");
const defaultLinkColor = new ConstantColor<LinkObject>("#AAA");

export default function MainPanel({ project, options }: MainPanelProps) {
  const [hoverID, setHoverID] = useState<string | null>(null);
  const { ref: graphRef } = useResizeDetector();

  const graph = useMemo(() => {
    const builder = new Builder(project.root.name);
    const visitor = filterVisitor(builder, project.root.name, options);

    project.visit(visitor);

    return builder.build();
  }, [project, options]);

  const hoverPredicate = useMemo(
    () =>
      hoverID
        ? new Predicate((nodeOrID) => toNodeID(nodeOrID) === hoverID)
        : NeverPredicate,
    [hoverID]
  );

  const baseNodeColor = useMemo(
    (): ColorMap<NodeObject> => createDepthColorMap(graph, "#0F0", "#00F"),
    [graph]
  );

  const stabilityMap = useMemo(() => StabilityMap.build(graph), [graph]);
  const nodeSize = useCallback(
    (node: NodeObject) => stabilityMap.valueOf(node),
    [stabilityMap]
  );

  const nodeColorScheme = useMemo(
    (): ColorMap<NodeObject> =>
      hoverID
        ? new PredicateColorMap(
            (node) => hoverPredicate.acceptNode(node),
            "#F00",
            "Under mouse cursor",
            baseNodeColor
          )
        : baseNodeColor,
    [hoverID, baseNodeColor, hoverPredicate]
  );

  const linkColorScheme = useMemo(() => {
    let linkColorScheme: ColorMap<LinkObject> = defaultLinkColor;

    if (hoverID) {
      linkColorScheme = new PredicateColorMap(
        (link) => hoverPredicate.acceptLink(link),
        "#F00",
        "Under mouse cursor",
        linkColorScheme
      );
    }

    return linkColorScheme;
  }, [hoverID, hoverPredicate]);

  const nodeColor = useCallback(
    (node: NodeObject) => nodeColorScheme.colorOf(node),
    [nodeColorScheme]
  );
  const linkcolor = useCallback(
    (link: LinkObject) => linkColorScheme.colorOf(link),
    [linkColorScheme]
  );

  return (
    <>
      <div id="list">
        <NodeList graph={graph} color={nodeColor} onHover={setHoverID} />
      </div>
      <div id="graph" ref={graphRef}>
        <ErrorBoundary>
          <GraphPanel
            graph={graph}
            nodeColor={nodeColor}
            nodeSize={nodeSize}
            linkColor={linkcolor}
            onHover={setHoverID}
            width={graphRef.current?.clientWidth}
            height={graphRef.current?.clientHeight}
          />
        </ErrorBoundary>
      </div>
      {nodeColorScheme && isColorScheme(nodeColorScheme) ? (
        <div id="legend">
          <ColorLegend scheme={nodeColorScheme} />
        </div>
      ) : null}
      {hoverID && (
        <div id="hover">
          <NodeInfo node={graph.node(hoverID)} />
        </div>
      )}
    </>
  );
}
