import { forceRadial } from "d3-force";
import { useMemo, useRef, useState } from "react";
import ForceGraph, {
  ForceGraphMethods,
  LinkObject,
  NodeObject,
} from "react-force-graph-2d";
import { Graph } from "../../model";
import { DepthMap } from "../../viewmodel";
import { GraphDataAdapter, toNodeID } from "../../viewmodel/graphview";

export interface GraphProps {
  graph: Graph;
  width: number | null;
  height: number | null;
  nodeColor: (node: NodeObject) => string;
  nodeSize: (node: NodeObject) => number;
  linkColor: (node: LinkObject) => string;
  onHover: (id: string | null) => void;
}

export default function GraphPanel({
  graph,
  nodeColor,
  nodeSize,
  linkColor,
  onHover,
  width,
  height,
}: GraphProps) {
  const [adapter] = useState<GraphDataAdapter>(() => new GraphDataAdapter());
  const ref = useRef<ForceGraphMethods>();
  const radial = useMemo(() => DepthMap.build(graph), [graph]);

  adapter.update(graph);

  if (ref.current) {
    ref.current.d3Force(
      "depth",
      forceRadial((node: NodeObject) => 100 * radial.valueOf(node)).strength(1)
    );
  }

  return (
    <ForceGraph
      ref={ref}
      width={width || 100}
      height={height || 100}
      graphData={adapter.graphData()}
      nodeColor={nodeColor}
      nodeVal={nodeSize}
      onNodeHover={(node) => onHover(node ? toNodeID(node) : null)}
      linkDirectionalArrowLength={8}
      linkColor={linkColor}
      linkCurvature={0.1}
      cooldownTicks={100}
      onEngineStop={() => ref.current?.zoomToFit(400)}
    />
  );
}
