import { toRgba } from "color2k";
import { ColorDef, isColorRange } from "../../viewmodel";
import "./ColorWatch.css";

export interface ColorWatchProps {
  color: ColorDef;
}

export default function ColorWatch({ color }: ColorWatchProps) {
  const background = isColorRange(color)
    ? `linear-gradient(to right, ${toRgba(color[0])}, ${toRgba(color[1])})`
    : color;

  return <span className="colorWatch" style={{ background }}></span>;
}
