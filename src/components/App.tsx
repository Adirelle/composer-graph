import { useState } from "react";
import { defaultFilterOptions, FilterOptions, Project } from "../model";
import "./App.css";
import ErrorBoundary from "./ErrorBoundary";
import Header from "./Header";
import MainPanel from "./MainPanel";

function App() {
  const [project, setProject] = useState<Project | undefined>();
  const [options, setOptions] = useState<FilterOptions>(defaultFilterOptions);

  return (
    <>
      <Header
        project={project}
        setProject={setProject}
        options={options}
        setOptions={setOptions}
      />
      {project ? (
        <ErrorBoundary>
          <MainPanel project={project} options={options} />
        </ErrorBoundary>
      ) : null}
    </>
  );
}

export default App;
