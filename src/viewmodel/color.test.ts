import {
  ColorMap,
  ColorMapping,
  ColorScheme,
  ConstantColor,
  PredicateColorMap,
  ValueMap,
} from "./color";

describe("ConstantColor", () => {
  const colorMap = new ConstantColor<number>("red");

  it.each([1, 2, 3])("should always return the same value", (input) =>
    expect(colorMap.colorOf(input)).toEqual("red")
  );
});

describe("PredicateColorMap", () => {
  const fallback: ColorMap<number> & ColorScheme = {
    colorOf: () => "blue",
    colors: () => [],
  };
  const c = new PredicateColorMap(
    (i: number) => i === 2,
    "red",
    "woohoo",
    fallback
  );

  it.each([1, 3])(
    "should use the fallback color map for unselected value",
    (input) => {
      fallback.colorOf = jest.fn(() => "blue");
      expect(c.colorOf(input)).toEqual("blue");
      expect(fallback.colorOf).toHaveBeenCalled();
    }
  );

  it("should return the right value", () =>
    expect(c.colorOf(2)).toEqual("red"));

  it("should list its color", () => {
    fallback.colors = jest.fn(() => [{ color: "blue", label: "fallback" }]);
    const colors = Array.from(c.colors());

    expect(colors).toEqual([
      { color: "blue", label: "fallback" },
      { color: "red", label: "woohoo" },
    ]);
  });
});

describe("ColorMapping", () => {
  it("should use", () => {
    const colorMap: ColorMap<string> = {
      colorOf: jest.fn((value: string) => value),
    };
    const valueMap: ValueMap<number, string> = {
      valueOf: jest.fn((input: number) => input.toString()),
    };
    const mapping = new ColorMapping(valueMap, colorMap);

    expect(mapping.colorOf(5)).toEqual("5");
    expect(valueMap.valueOf).toHaveBeenCalledWith(5);
    expect(colorMap.colorOf).toHaveBeenCalledWith("5");
  });
});
