import { Graph, NodeID } from "../model";
import { DepthMap } from "./depth";

describe("buildDepthMap", () => {
  const graph = Graph.build("pkg/root", {
    "pkg/root": ["pkg/foo", "pkg/quz"],
    "pkg/foo": ["pkg/bar", "pkg/quz"],
    "pkg/bar": {},
    "pkg/quz": {},
  });
  const depths = DepthMap.build(graph);

  it.each<[NodeID, number]>([
    ["pkg/root", 0],
    ["pkg/foo", 1],
    ["pkg/quz", 1],
    ["pkg/bar", 2],
  ])("the node %s should have a depth of %d", (id, depth) => {
    expect(depths.valueOf(id)).toEqual(depth);
  });

  it("should have the right min depth", () => {
    expect(depths.min).toBe(0);
  });

  it("should have the right max depth", () => {
    expect(depths.max).toBe(2);
  });
});
