import { NodeObject } from "react-force-graph-2d";
import { Graph, NodeID } from "../model";
import { ColorMapping, DiscreteColorScale, ValueMap } from "./color";
import { toNodeID } from "./graphview";

export class DepthMap implements ValueMap<NodeObject | NodeID, number> {
  readonly min: number;

  readonly max: number;

  private constructor(private readonly depths: Map<NodeID, number>) {
    const allDepths = Array.from(depths.values());

    this.min = Math.min(Infinity, ...allDepths);
    this.max = Math.max(-Infinity, ...allDepths);
  }

  valueOf(nodeOrID: NodeObject | NodeID): number {
    return this.depths.get(toNodeID(nodeOrID)) || 0;
  }

  static build(graph: Graph): DepthMap {
    const depths = new Map<NodeID, number>();

    const depthOf = (id: NodeID): number => {
      if (depths.has(id)) {
        return depths.get(id)!;
      }

      const { linkedFrom } = graph.node(id);

      depths.set(id, 0);
      let depth = 0;
      if (linkedFrom.size > 0) {
        depth = 1 + Math.min(...Array.from(linkedFrom).map(depthOf));
        depths.set(id, depth);
      }

      return depth;
    };

    for (const leaf of graph.nodes()) {
      depthOf(leaf.id);
    }

    console.debug(depths);

    return new DepthMap(depths);
  }
}

export function createDepthColorMap(
  graph: Graph,
  rootColor: string,
  farthestColor: string
) {
  const depthMap = DepthMap.build(graph);

  return new ColorMapping(
    depthMap,
    new DiscreteColorScale(
      depthMap.min,
      rootColor,
      depthMap.max,
      farthestColor,
      (depth) =>
        depth === 0
          ? "Root"
          : depth === 1
          ? "Direct dependency"
          : `Indirect dependency (distance from root: ${depth})`
    )
  );
}
