import { LinkObject, NodeObject } from "react-force-graph-2d";
import { NodeID } from "../model";
import { toNodeID } from "./graphview";

export interface NodePredicate {
  acceptNode(node: NodeObject): boolean;
}

export interface LinkPredicate {
  acceptLink(link: LinkObject): boolean;
}

export class Predicate implements NodePredicate, LinkPredicate {
  constructor(
    private readonly predicate: (node: NodeObject | NodeID) => boolean
  ) {}

  acceptNode(node: NodeObject): boolean {
    return this.predicate(toNodeID(node));
  }

  acceptLink({ source, target }: LinkObject): boolean {
    return this.predicate(toNodeID(source)) || this.predicate(toNodeID(target));
  }
}

export const NeverPredicate: LinkPredicate & NodePredicate = {
  acceptNode: () => false,
  acceptLink: () => false,
};
