import { GraphData, LinkObject, NodeObject } from "react-force-graph-2d";
import { Graph, Link, Node, NodeID } from "../model";

export function toNodeID(
  what: NodeObject | string | number | undefined
): NodeID {
  if (typeof what === "string") {
    return what;
  }

  if (typeof what === "object" && what !== null && "id" in what) {
    return toNodeID(what.id);
  }

  throw new Error(`invalid node reference: ${what}`);
}

export class GraphDataAdapter {
  private graph?: Graph;

  private data: GraphData = { nodes: [], links: [] };

  update(graph: Graph): void {
    if (graph === this.graph) {
      return;
    }

    const oldNodes = new Map(this.data.nodes.map((node) => [node.id, node]));
    const nodes: NodeObject[] = [];
    const links: LinkObject[] = [];

    for (const { id, linksTo } of graph.nodes()) {
      nodes.push(oldNodes.get(id) || { id });
      links.push(
        ...linksTo.map(({ source, target, type }) => ({ source, target, type }))
      );
    }

    this.graph = graph;
    this.data = { nodes, links };
  }

  graphData(): GraphData {
    return this.data;
  }

  node(nodeOrId: NodeObject | string | number | undefined): Node {
    const node = this.graph?.node(toNodeID(nodeOrId));
    if (!node) {
      throw new Error(`Unknown node: ${nodeOrId}`);
    }

    return node;
  }

  link(link: LinkObject | Link): Link {
    const source = this.node(link.source);
    const targetID = toNodeID(link.target);
    const type = "type" in link ? link.type : "prod";
    const foundLink = source.linksTo.find(
      (l) => l.target === targetID && l.type === type
    );
    if (foundLink) {
      return foundLink;
    }

    throw new Error(`link not found: ${link.source} -> ${link.target}`);
  }
}
