import { Graph } from "../model";
import { GraphDataAdapter, toNodeID } from "./graphview";

describe("toNodeID", () => {
  it.each([
    ["root/pkg", "root/pkg"],
    [{ id: "root/pkg" }, "root/pkg"],
  ])("should extract the node id of valid inputs", (input, expected) => {
    expect(toNodeID(input)).toBe(expected);
  });

  it.each([[5], [undefined]])("should through on invalid vlaue", (input) => {
    expect(() => toNodeID(input)).toThrow();
  });
});

describe("GraphDataAdapter", () => {
  const graph = Graph.build("root/pkg", {
    "root/pkg": [
      "direct/prod",
      "direct/both",
      { target: "direct/both", type: "dev" },
    ],
    "direct/prod": {},
    "direct/both": ["direct/bar"],
    "direct/bar": {},
  });
  let adapter: GraphDataAdapter;

  beforeAll(() => {
    adapter = new GraphDataAdapter();
    adapter.update(graph);
  });

  it("should build a GraphData from input", () => {
    const data = adapter.graphData();
    const node = data.nodes.find(({ id }) => id === "root/pkg");
    const link = data.links.find(
      ({ source, target }) => source === "root/pkg" && target === "direct/prod"
    );

    expect(node).toBeDefined();
    expect(link).toBeDefined();
  });

  it("should keep existing nodes across update", () => {
    const node = adapter.graphData().nodes.find(({ id }) => id === "root/pkg");

    adapter.update(graph);

    const newNode = adapter
      .graphData()
      .nodes.find(({ id }) => id === "root/pkg");

    expect(node).toBeDefined();
    expect(newNode).toBe(node);
  });

  it("should handle multiple links between nodes", () => {
    const numLinks = adapter
      .graphData()
      .links.filter(
        ({ source, target }) =>
          source === "root/pkg" && target === "direct/both"
      );

    expect(numLinks.length).toBe(2);
  });

  it(".node should resolve node", () => {
    const node = adapter.node("root/pkg");

    expect(node).toBeDefined();
    expect(node?.id).toBe("root/pkg");
  });

  it(".node should throw on unexisting node", () => {
    expect(() => adapter.node("donot/exist")).toThrow();
  });

  it(".link should resolve link", () => {
    const link = adapter.link({
      source: "root/pkg",
      target: "direct/prod",
      type: "prod",
    });

    expect(link).toBeDefined();
    expect(link?.source).toBe("root/pkg");
    expect(link?.target).toBe("direct/prod");
  });

  it(".link should throw on unexisting link", () => {
    expect(() =>
      adapter.link({ source: "root/pkg", target: "foo/bar" })
    ).toThrow();
  });
});
