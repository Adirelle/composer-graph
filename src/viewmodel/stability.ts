import { NodeObject } from "react-force-graph-2d";
import { Graph, NodeID } from "../model";
import { ColorMapping, ContinuousColorScale, ValueMap } from "./color";
import { toNodeID } from "./graphview";

export class StabilityMap implements ValueMap<NodeObject | NodeID, number> {
  readonly min: number;

  readonly max: number;

  private constructor(private readonly stabilities: Map<NodeID, number>) {
    const allStabilities = Array.from(stabilities.values());

    this.min = Math.min(Infinity, ...allStabilities);
    this.max = Math.max(-Infinity, ...allStabilities);
  }

  valueOf(nodeOrID: NodeObject | NodeID): number {
    return this.stabilities.get(toNodeID(nodeOrID)) || 0;
  }

  static build(graph: Graph): StabilityMap {
    const dependentNodes = new Map<NodeID, Set<NodeID>>();

    const walk = (id: NodeID) => {
      if (dependentNodes.has(id)) {
        return dependentNodes.get(id)!;
      }

      const { linkedFrom } = graph.node(id);
      const ids = new Set(linkedFrom);

      dependentNodes.set(id, ids);
      linkedFrom.forEach((up) => walk(up).forEach((id) => ids.add(id)));

      return ids;
    };

    for (const leaf of graph.nodes()) {
      walk(leaf.id);
    }

    return new StabilityMap(
      new Map(
        Array.from(dependentNodes.entries()).map(([id, ids]) => [id, ids.size])
      )
    );
  }
}

export function createStabilityColorMap(
  graph: Graph,
  minColor: string,
  maxColor: string
) {
  const stabilityMap = StabilityMap.build(graph);

  return new ColorMapping(
    (node: NodeObject) => Math.log(1 + stabilityMap.valueOf(toNodeID(node))),
    new ContinuousColorScale(
      Math.log(1 + stabilityMap.min),
      minColor,
      Math.log(1 + stabilityMap.max),
      maxColor,
      "Stability"
    )
  );
}
