import { mix } from "color2k";

export interface ValueMap<K, V> {
  valueOf(k: K): V;
}

export interface ColorMap<T> {
  colorOf(item: T): string;
}

export type SingleColor = string;
export type ColorRange = [SingleColor, SingleColor];
export type ColorDef = SingleColor | ColorRange;
export type ColorLegend = {
  color: ColorDef;
  label: string;
};

export function isColorRange(what: ColorDef): what is ColorRange {
  return what instanceof Array;
}

export interface ColorScheme {
  colors(): Iterable<ColorLegend>;
}

export function isColorScheme<T>(
  c: ColorMap<T>
): c is ColorMap<T> & ColorScheme {
  return "colors" in c;
}

export class ConstantColor<T> implements ColorMap<T> {
  constructor(readonly color: string) {}

  colorOf(_item: T): string {
    return this.color;
  }
}

export class PredicateColorMap<T> implements ColorMap<T>, ColorScheme {
  constructor(
    private readonly predicate: (item: T) => boolean,
    private readonly color: string,
    private readonly label: string,
    private readonly fallback: ColorMap<T>
  ) {}

  *colors(): Iterable<ColorLegend> {
    if (isColorScheme(this.fallback)) {
      yield* this.fallback.colors();
    }

    yield { color: this.color, label: this.label };
  }

  colorOf(item: T): string {
    return this.predicate(item) ? this.color : this.fallback.colorOf(item);
  }
}

export class ColorMapping<T, U> implements ColorMap<T>, ColorScheme {
  constructor(
    private readonly valueMap: ValueMap<T, U>,
    private readonly colorMap: ColorMap<U>
  ) {}

  colorOf(item: T): string {
    return this.colorMap.colorOf(this.valueMap.valueOf(item));
  }

  colors(): Iterable<ColorLegend> {
    return isColorScheme(this.colorMap) ? this.colorMap.colors() : [];
  }
}

export class DiscreteColorScale implements ColorMap<number>, ColorScheme {
  private readonly range: number;

  constructor(
    private readonly min: number,
    private readonly minColor: string,
    max: number,
    private readonly maxColor: string,
    private readonly label = (value: number): string => value.toString(),
    private readonly step = 1
  ) {
    this.range = max - min;
  }

  *colors(): Iterable<ColorLegend> {
    for (let value = this.min; value <= this.range; value += this.step) {
      yield { color: this.colorOf(value), label: this.label(value) };
    }
  }

  colorOf(item: number): string {
    const ratio =
      (this.step * Math.round((item - this.min) / this.step)) / this.range;

    return mix(this.minColor, this.maxColor, ratio);
  }
}

export class ContinuousColorScale implements ColorMap<number>, ColorScheme {
  private readonly range: number;

  constructor(
    private readonly min: number,
    private readonly minColor: string,
    max: number,
    private readonly maxColor: string,
    private readonly label: string
  ) {
    this.range = max - min;
  }

  colors(): Iterable<ColorLegend> {
    return [{ color: [this.minColor, this.maxColor], label: this.label }];
  }

  colorOf(item: number): string {
    const ratio = (item - this.min) / this.range;

    return mix(this.minColor, this.maxColor, ratio);
  }
}
