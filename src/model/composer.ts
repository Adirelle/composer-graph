import { LinkType, NodeType, Visitor } from "./graph";

export type PackageLinks = Record<string, string>;

export interface Autoload {
  "psr-0"?: Record<string, string>;
  "psr-4"?: Record<string, string>;
  classmap?: string[];
  files?: string[];
}

export interface Package {
  name: string;
  version?: string;
  type?: string;
  homepage?: string;
  readme?: string;
  time?: string;
  keywords?: string[];
  license?: string | string[];
  description?: string;
  authors?: Array<{
    name: string;
    email?: string;
    homepage?: string;
    role?: string;
  }>;
  support?: {
    email?: string;
    issues?: string;
    forum?: string;
    wiki?: string;
    irc?: string;
    source?: string;
    docs?: string;
    rss?: string;
    chat?: string;
  };
  funding?: { type: string; url: string };
  require?: PackageLinks;
  "require-dev"?: PackageLinks;
  conflict?: PackageLinks;
  replace?: PackageLinks;
  provide?: PackageLinks;
  suggest?: Record<string, string>;
  autoload?: Autoload;
  "autoload-dev"?: Autoload;
  abandoned?: boolean;
}

export type RootPackage = Package;

export interface LockedPackage extends Package {
  source?: {
    type: string;
    url: string;
    reference: string;
  };
  dist?: {
    type: string;
    url: string;
    reference: string;
    shasum: string;
  };
}

export interface LockFile {
  packages: LockedPackage[];
}

export class Project {
  readonly packages: ReadonlyMap<string, Package>;

  constructor(readonly root: RootPackage, lockFile?: LockFile) {
    const pkg = new Map(
      Array.from((lockFile?.packages || []).map((pkg) => [pkg.name, pkg]))
    );

    pkg.set(root.name, root);
    this.packages = pkg;
  }

  visit(visitor: Visitor) {
    new ProjectWalker(this.root.name, this.packages, visitor).walk();
  }
}

// php | hhvm | lib-* | ext-* | * | */*
const idRe =
  /^(?:php|hhvm|lib-(\w+)|ext-(\w+)|([a-z0-9_.-]+(\/[a-z0-9_.-]+)?))$/;

export function guessNodeTypeFromId(id: string): NodeType {
  const [valid, lib, ext, pkgOrNmspc, pkg] = idRe.exec(id) || [false];
  if (!valid) {
    throw new Error(`invalid identifier: ${id}`);
  } else if (pkg) {
    return "package";
  } else if (pkgOrNmspc) {
    return "namespace";
  } else if (ext) {
    return "extension";
  } else if (lib) {
    return "library";
  } else {
    return "language";
  }
}

class ProjectWalker {
  private walked = new Set<string>();

  constructor(
    private readonly rootName: string,
    private readonly packages: ReadonlyMap<string, Package>,
    private readonly visitor: Visitor
  ) {}

  walk(): void {
    this.visitNode(this.rootName);
  }

  private visitNode(id: string): void {
    if (this.walked.has(id)) {
      return;
    }

    this.walked.add(id);

    const type = guessNodeTypeFromId(id);

    if (type === "extension" || type === "library") {
      this.visitor.visitNode(id, "*", type, () => {
        this.visitor.visitLink(id, "php", "*", "prod", () =>
          this.visitNode("php")
        );
      });

      return;
    }

    if (type === "package" && this.packages.has(id)) {
      const {
        version,
        require,
        "require-dev": requireDev,
      } = this.packages.get(id)!;
      const [namespace] = id.split("/");

      this.visitor.visitNode(id, version || "*", type, () => {
        this.visitor.visitLink(id, namespace, "*", "prod", () =>
          this.visitNode(namespace)
        );

        if (require) {
          this.visitLinks(id, require, "prod");
        }

        if (requireDev) {
          this.visitLinks(id, requireDev, "dev");
        }
      });

      return;
    }

    this.visitor.visitNode(id, "*", type, () => void 0);
  }

  private visitLinks(
    source: string,
    targets: PackageLinks,
    type: LinkType
  ): void {
    for (const [target, constraint] of Object.entries(targets)) {
      this.visitor.visitLink(source, target, constraint, type, () =>
        this.visitNode(target)
      );
    }
  }
}
