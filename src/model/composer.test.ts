import { guessNodeTypeFromId, Project } from "./composer";
import { LinkType, NodeType, Visitor } from "./graph";

describe("guessNodeTypeFromId", () => {
  it.each<[string, NodeType]>([
    ["php", "language"],
    ["hhvm", "language"],
    ["lib-curl", "library"],
    ["ext-ldap", "extension"],
    ["symfony", "namespace"],
    ["symfony/symfony", "package"],
    ["symfony/with_underscore", "package"],
  ])("should identify '%s' as '%s'", (id, expected) => {
    expect(guessNodeTypeFromId(id)).toBe(expected);
  });

  it("should throw on empty value", () => {
    expect(() => guessNodeTypeFromId("")).toThrow();
  });

  it("should throw on invalid value", () => {
    expect(() => guessNodeTypeFromId("Caps")).toThrow();
  });
});

describe("visitProject", () => {
  const project = new Project(
    {
      name: "root/pkg",
      version: "1.0",
      require: {
        php: "^8.2",
        "ext-ldap": "*",
        "direct/prod": "^5.0",
        "direct/both": "^8.0",
      },
      "require-dev": {
        "direct/dev": "^6.0",
        "direct/both": "^8.0",
      },
    },
    {
      packages: [
        {
          name: "direct/prod",
          version: "5.1",
          require: {
            php: "^8.2",
            "indirect/prod-prod": "^5.0",
            "indirect/both": "^3.0",
          },
          "require-dev": {
            "indirect/prod-dev": "^5.0",
          },
        },
        {
          name: "direct/dev",
          version: "6.8",
          require: {
            php: "^8.0",
            "indirect/dev-prod": "^5.0",
            "indirect/both": "^3.0",
          },
          "require-dev": {
            "indirect/dev-dev": "^5.0",
          },
        },
      ],
    }
  );

  let visitor: Visitor;

  beforeEach(() => {
    visitor = {
      visitNode: jest.fn((_id, _version, _type, visitLinks) => visitLinks()),
      visitLink: jest.fn((_source, _target, _constraint, _type, visitTarget) =>
        visitTarget()
      ),
    };
    project.visit(visitor);
  });

  it.each<[string, string, NodeType]>([
    ["root/pkg", "1.0", "package"],
    ["php", "*", "language"],
    ["direct/prod", "5.1", "package"],
    ["direct/dev", "6.8", "package"],
    ["indirect/prod-prod", "*", "package"],
    ["indirect/prod-dev", "*", "package"],
    ["indirect/both", "*", "package"],
    ["indirect/dev-prod", "*", "package"],
    ["indirect/dev-dev", "*", "package"],
    ["indirect/both", "*", "package"],
  ])("should visit node '%s'", (id, version, type) => {
    expect(visitor.visitNode).toBeCalledWith(
      id,
      version,
      type,
      expect.anything()
    );
  });

  it.each<[string, string, string, LinkType]>([
    ["root/pkg", "php", "^8.2", "prod"],
    ["root/pkg", "ext-ldap", "*", "prod"],
    ["root/pkg", "direct/prod", "^5.0", "prod"],
    ["root/pkg", "direct/both", "^8.0", "prod"],
    ["root/pkg", "direct/dev", "^6.0", "dev"],
    ["root/pkg", "direct/both", "^8.0", "dev"],
    ["direct/prod", "php", "^8.2", "prod"],
    ["direct/prod", "indirect/prod-prod", "^5.0", "prod"],
    ["direct/prod", "indirect/both", "^3.0", "prod"],
    ["direct/prod", "indirect/prod-dev", "^5.0", "dev"],
    ["direct/dev", "php", "^8.0", "prod"],
    ["direct/dev", "indirect/dev-prod", "^5.0", "prod"],
    ["direct/dev", "indirect/both", "^3.0", "prod"],
    ["direct/dev", "indirect/dev-dev", "^5.0", "dev"],
  ])("should visit link '%s -> %s'", (source, target, constraint, type) => {
    expect(visitor.visitLink).toBeCalledWith(
      source,
      target,
      constraint,
      type,
      expect.anything()
    );
  });

  it("should not visit filtered out nodes", () => {
    visitor = {
      visitNode: jest.fn((_id, _version, _type, visitLinks) => visitLinks()),
      visitLink: jest.fn((_source, _target, _constraint, type, visitTarget) =>
        type === "prod" ? visitTarget() : void 0
      ),
    };
    project.visit(visitor);

    expect(visitor.visitNode).not.toBeCalledWith(
      "direct/dev",
      "6.8",
      "package",
      expect.anything()
    );
    expect(visitor.visitNode).not.toBeCalledWith(
      "indirect/prod-dev",
      "*",
      "package",
      expect.anything()
    );
    expect(visitor.visitNode).not.toBeCalledWith(
      "indirect/dev-prod",
      "*",
      "package",
      expect.anything()
    );
    expect(visitor.visitNode).not.toBeCalledWith(
      "indirect/dev-dev",
      "*",
      "package",
      expect.anything()
    );
  });
});
