import { Link, LinkType, Node, NodeID, NodeType } from "./types";

type GraphSpec<T extends NodeID> = Record<T, NodeSpec<T>>;

type NodeSpec<T extends NodeID> =
  | { version?: string; type?: NodeType; links?: LinkSpec<T>[] }
  | LinkSpec<T>[];

type LinkSpec<T extends NodeID> =
  | T
  | { target: T; constraint?: string; type?: LinkType };

export class Graph {
  constructor(
    readonly rootID: NodeID,
    private readonly nodeMap: ReadonlyMap<NodeID, Node>
  ) {
    if (!this.nodeMap.has(rootID)) {
      throw new Error(`root ${rootID} must be in the node map`);
    }

    this.nodeMap.forEach(({ id, linksTo, linkedFrom }, mapId) => {
      if (mapId !== id) {
        throw new Error(`incoherent node/map id: ${id}/${mapId}`);
      }

      linksTo.forEach(({ source, target, type }) => {
        if (source !== id || !this.nodeMap.get(target)?.linkedFrom?.has(id)) {
          throw new Error(`invalid link: ${source} -> ${target} (${type})`);
        }
      });
      linkedFrom.forEach((source) => {
        if (
          !this.nodeMap
            .get(source)
            ?.linksTo?.find(({ target }) => target === id)
        ) {
          throw new Error(`invalid linkedFrom: ${id} <- ${source}`);
        }
      });
    });
  }

  get root(): Node {
    return this.nodeMap.get(this.rootID)!;
  }

  get size(): number {
    return this.nodeMap.size;
  }

  nodes(): IterableIterator<Node> {
    return this.nodeMap.values();
  }

  leaves(): Iterable<Node> {
    return Array.from(this.nodeMap.values()).filter(
      ({ linksTo }) => linksTo.length === 0
    );
  }

  node(id: NodeID): Node {
    const node = this.nodeMap.get(id);
    if (!node) {
      console.error("node id=", id);
      throw new Error(`node does not exist: ${id}`);
    }

    return node;
  }

  hasNode(id: NodeID): boolean {
    return this.nodeMap.has(id);
  }

  *links(): Iterator<Link> {
    for (const { linksTo } of this.nodeMap.values()) {
      yield* linksTo;
    }
  }

  static build<T extends NodeID>(root: T, graphSpec: GraphSpec<T>): Graph {
    const nodes = new Map<NodeID, Node>();

    for (let [source, nodeSpec] of Object.entries<NodeSpec<T>>(graphSpec)) {
      if (nodeSpec instanceof Array) {
        nodeSpec = { links: nodeSpec };
      }

      nodes.set(source, {
        id: source,
        version: nodeSpec.version || "*",
        type: nodeSpec.type || "package",
        linksTo:
          nodeSpec.links?.map((link) =>
            typeof link === "string"
              ? { source, target: link, constraint: "*", type: "prod" }
              : {
                  source,
                  target: link.target,
                  constraint: link.constraint || "*",
                  type: link.type || "prod",
                }
          ) || [],
        linkedFrom: new Set(),
      });
    }

    nodes.forEach(({ id: source, linksTo }) => {
      linksTo.forEach(({ target }) => {
        nodes.get(target)?.linkedFrom?.add(source);
      });
    });

    return new Graph(root, nodes);
  }
}
