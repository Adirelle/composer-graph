import { Graph } from "./graph";
import { LinkType, NodeID, NodeType, Visitor } from "./types";

class BuilderNode {
  linksTo: BuilderLink[] = [];

  linkedFrom = new Set<NodeID>();

  get label(): string {
    if (this.type !== "namespace" && this.version !== "*") {
      return `${this.id}:${this.version}`;
    }

    return this.id;
  }

  get tooltip(): string {
    if (this.type !== "namespace" && this.version !== "*") {
      return `${this.id}:${this.version}`;
    }

    return this.id;
  }

  constructor(
    readonly id: NodeID,
    readonly version: string,
    readonly type: NodeType
  ) {}

  addLinkTo(target: BuilderNode, constraint: string, type: LinkType): void {
    const link = new BuilderLink(this.id, target.id, constraint, type);

    this.linksTo.push(link);
    target.linkedFrom.add(this.id);
  }
}

class BuilderLink {
  constructor(
    readonly source: NodeID,
    readonly target: NodeID,
    readonly constraint: string,
    readonly type: LinkType
  ) {}

  get label(): string | undefined {
    return this.constraint !== "*" ? this.constraint : undefined;
  }
}

export class Builder implements Visitor {
  private readonly nodes = new Map<NodeID, BuilderNode>();

  constructor(private readonly root: NodeID) {}

  build(): Graph {
    return new Graph(this.root, this.nodes);
  }

  visitNode(
    id: string,
    version: string,
    type: NodeType,
    visitLinks: () => void
  ): void {
    if (this.nodes.has(id)) {
      return;
    }

    this.nodes.set(id, new BuilderNode(id, version, type));
    visitLinks();
  }

  visitLink(
    sourceId: string,
    targetId: string,
    constraint: string,
    type: LinkType,
    visitTarget: () => void
  ): void {
    const source = this.nodes.get(sourceId);
    if (!source) {
      return;
    }

    visitTarget();

    const target = this.nodes.get(targetId);
    if (target) {
      source.addLinkTo(target, constraint, type);
    }
  }
}
