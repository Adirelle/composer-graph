import { Graph } from "./graph";

describe("Graph", () => {
  const graph = Graph.build("root/pkg", {
    "root/pkg": [
      "direct/prod",
      "direct/both",
      { target: "direct/both", type: "dev" },
    ],
    "direct/prod": {},
    "direct/both": ["direct/bar"],
    "direct/bar": ["direct/foo"],
    "direct/foo": ["direct/both"],
  });
  const nodeList = [
    "root/pkg",
    "direct/prod",
    "direct/both",
    "direct/bar",
    "direct/foo",
  ];

  it.each(nodeList)(".nodes() should contain %s", (id) => {
    const nodes = Array.from(graph.nodes());

    expect(nodes.find((node) => node.id === id)).toBeDefined();
  });

  it.each(nodeList)(".node('%s') should return the node", (id) => {
    expect(graph.node(id)?.id).toEqual(id);
  });

  it(".node should throw on unknown node", () => {
    expect(() => graph.node("not/there")).toThrowError();
  });
});
