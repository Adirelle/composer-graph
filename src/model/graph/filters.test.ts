import {
  NoDevVisitor,
  NoIndirectLinkVisitor,
  NoNamespaceVisitor,
  NoPlatformVisitor,
  OnlyRootDevVisitor,
} from "./filters";
import { NodeType, Visitor } from "./types";

function mockVisitor(): Visitor {
  return {
    visitNode: jest.fn((_id, _version, _type, visitLinks) => visitLinks()),
    visitLink: jest.fn((_source, _target, _constraint, _type, visitTarget) =>
      visitTarget()
    ),
  };
}

describe("NoDevVisitor", () => {
  let target: Visitor;
  let Visitor: Visitor;

  beforeEach(() => {
    target = mockVisitor();
    Visitor = new NoDevVisitor(target);
  });

  it("should accept prod links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("a", "b", "*", "prod", visitTarget);

    expect(visitTarget).toHaveBeenCalled();
    expect(target.visitLink).toHaveBeenCalled();
  });

  it("should ignore dev links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("a", "b", "*", "dev", visitTarget);

    expect(visitTarget).not.toHaveBeenCalled();
    expect(target.visitLink).not.toHaveBeenCalled();
  });
});

describe("OnlyRootDevVisitor", () => {
  let target: Visitor;
  let Visitor: Visitor;

  beforeEach(() => {
    target = mockVisitor();
    Visitor = new OnlyRootDevVisitor("root", target);
  });

  it("should accept prod links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("a", "b", "*", "prod", visitTarget);

    expect(visitTarget).toHaveBeenCalled();
    expect(target.visitLink).toHaveBeenCalled();
  });

  it("should accept root dev links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("root", "b", "*", "dev", visitTarget);

    expect(visitTarget).toHaveBeenCalled();
    expect(target.visitLink).toHaveBeenCalled();
  });

  it("should ignore non-root dev links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("a", "b", "*", "dev", visitTarget);

    expect(visitTarget).not.toHaveBeenCalled();
    expect(target.visitLink).not.toHaveBeenCalled();
  });
});

describe("NoPlatformVisitor", () => {
  let target: Visitor;
  let Visitor: Visitor;

  beforeEach(() => {
    target = mockVisitor();
    Visitor = new NoPlatformVisitor(target);
  });

  it.each<NodeType>(["namespace", "package"])(
    "should accept %s nodes",
    (type: NodeType) => {
      const visitLinks = jest.fn();

      Visitor.visitNode("root", "*", type, visitLinks);

      expect(visitLinks).toHaveBeenCalled();
      expect(target.visitNode).toHaveBeenCalled();
    }
  );

  it.each<NodeType>(["language", "extension", "library"])(
    "should ignore %s nodes",
    (type: NodeType) => {
      const visitLinks = jest.fn();

      Visitor.visitNode("root", "*", type, visitLinks);

      expect(visitLinks).not.toHaveBeenCalled();
      expect(target.visitLink).not.toHaveBeenCalled();
    }
  );
});

describe("NoNamespaceVisitor", () => {
  let target: Visitor;
  let Visitor: Visitor;

  beforeEach(() => {
    target = mockVisitor();
    Visitor = new NoNamespaceVisitor(target);
  });

  it.each<NodeType>(["package", "language", "extension", "library"])(
    "should accept %s nodes",
    (type: NodeType) => {
      const visitLinks = jest.fn();

      Visitor.visitNode("root", "*", type, visitLinks);

      expect(visitLinks).toHaveBeenCalled();
      expect(target.visitNode).toHaveBeenCalled();
    }
  );

  it.each<NodeType>(["namespace"])(
    "should ignore %s nodes",
    (type: NodeType) => {
      const visitLinks = jest.fn();

      Visitor.visitNode("root", "*", type, visitLinks);

      expect(visitLinks).not.toHaveBeenCalled();
      expect(target.visitLink).not.toHaveBeenCalled();
    }
  );
});

describe("NoIndirectLinkVisitor", () => {
  let target: Visitor;
  let Visitor: Visitor;

  beforeEach(() => {
    target = mockVisitor();
    Visitor = new NoIndirectLinkVisitor("root", target);
  });

  it("should accept direct prod links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("root", "b", "*", "prod", visitTarget);

    expect(visitTarget).toHaveBeenCalled();
    expect(target.visitLink).toHaveBeenCalled();
  });

  it("should accept direct dev links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("root", "b", "*", "dev", visitTarget);

    expect(visitTarget).toHaveBeenCalled();
    expect(target.visitLink).toHaveBeenCalled();
  });

  it("should ignore indirect prod links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("a", "b", "*", "prod", visitTarget);

    expect(visitTarget).not.toHaveBeenCalled();
    expect(target.visitLink).not.toHaveBeenCalled();
  });

  it("should ignore indirect dev links", () => {
    const visitTarget = jest.fn();

    Visitor.visitLink("a", "b", "*", "dev", visitTarget);

    expect(visitTarget).not.toHaveBeenCalled();
    expect(target.visitLink).not.toHaveBeenCalled();
  });
});
