import { LinkType, NodeType, Visitor } from "./types";

abstract class FilteredVisitor implements Visitor {
  constructor(private readonly decorated: Visitor) {}

  visitNode(
    id: string,
    version: string,
    type: NodeType,
    visitLinks: () => void
  ): void {
    if (this.acceptNode(id, version, type)) {
      this.decorated.visitNode(id, version, type, visitLinks);
    }
  }

  visitLink(
    source: string,
    target: string,
    constraint: string,
    type: LinkType,
    visitTarget: () => void
  ): void {
    if (this.acceptLink(source, target, constraint, type)) {
      this.decorated.visitLink(source, target, constraint, type, visitTarget);
    }
  }

  protected acceptNode(
    _id: string,
    _version: string,
    _type: NodeType
  ): boolean {
    return true;
  }

  protected acceptLink(
    _source: string,
    _target: string,
    _constraint: string,
    _type: LinkType
  ): boolean {
    return true;
  }
}

export class OnlyRootDevVisitor extends FilteredVisitor {
  constructor(private readonly rootName: string, decorated: Visitor) {
    super(decorated);
  }

  protected override acceptLink(
    source: string,
    _target: string,
    _constraint: string,
    type: LinkType
  ): boolean {
    return source === this.rootName || type !== "dev";
  }
}

export class NoDevVisitor extends FilteredVisitor {
  protected override acceptLink(
    _source: string,
    _target: string,
    _constraint: string,
    type: LinkType
  ): boolean {
    return type !== "dev";
  }
}

export class NoPlatformVisitor extends FilteredVisitor {
  protected override acceptNode(
    _id: string,
    _version: string,
    type: NodeType
  ): boolean {
    return type !== "language" && type !== "library" && type !== "extension";
  }
}

export class NoNamespaceVisitor extends FilteredVisitor {
  protected override acceptNode(
    _id: string,
    _version: string,
    type: NodeType
  ): boolean {
    return type !== "namespace";
  }
}

export class NoIndirectLinkVisitor extends FilteredVisitor {
  constructor(private readonly rootName: string, decorated: Visitor) {
    super(decorated);
  }

  protected override acceptLink(
    source: string,
    _target: string,
    _constraint: string,
    _type: LinkType
  ): boolean {
    return source === this.rootName;
  }
}

export interface FilterOptions {
  noDev: boolean;
  noPlatform: boolean;
  noNamespaces: boolean;
  noIndirect: boolean;
}

export const defaultFilterOptions: FilterOptions = {
  noDev: false,
  noPlatform: false,
  noNamespaces: false,
  noIndirect: false,
};

export function filterVisitor(
  visitor: Visitor,
  rootName: string,
  { noDev, noPlatform, noNamespaces, noIndirect }: FilterOptions
): Visitor {
  if (noDev) {
    visitor = new NoDevVisitor(visitor);
  } else {
    visitor = new OnlyRootDevVisitor(rootName, visitor);
  }

  if (noPlatform) {
    visitor = new NoPlatformVisitor(visitor);
  }

  if (noNamespaces) {
    visitor = new NoNamespaceVisitor(visitor);
  }

  if (noIndirect) {
    visitor = new NoIndirectLinkVisitor(rootName, visitor);
  }

  return visitor;
}
