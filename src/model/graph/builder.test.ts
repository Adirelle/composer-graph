import { Builder } from "./builder";

describe("Builder", () => {
  it("should create visited node", () => {
    const builder = new Builder("root/pkg");
    const next = jest.fn();

    builder.visitNode("root/pkg", "1.0", "package", next);
    const graph = builder.build();
    const root = graph.node(graph.rootID);

    expect(next).toBeCalled();
    expect(root).toBeDefined();
    expect(root?.version).toBe("1.0");
    expect(root?.type).toBe("package");
  });

  it("should create visited link", () => {
    const builder = new Builder("root/pkg");
    const next = jest.fn();

    builder.visitNode("root/pkg", "1.0", "package", next);
    builder.visitNode("direct/prod", "1.0", "package", next);
    builder.visitLink("root/pkg", "direct/prod", "^1.0", "prod", next);

    const graph = builder.build();
    const root = graph.node(graph.rootID);
    const dep = graph.node("direct/prod");

    expect(next).toBeCalled();
    expect(root).toBeDefined();
    expect(dep).toBeDefined();
    expect(root?.linksTo).toContainEqual({
      source: root!.id,
      target: dep!.id,
      constraint: "^1.0",
      type: "prod",
    });
  });

  it("should not create links to unvisted node", () => {
    const builder = new Builder("root/pkg");
    const next = () => void 0;

    builder.visitNode("root/pkg", "1.0", "package", next);
    builder.visitNode("a", "1.0", "package", next);

    builder.visitLink("a", "b", "^1.0", "prod", next);

    const graph = builder.build();

    expect(graph.hasNode("b")).toBeFalsy();
  });

  it("should not create links from unvisted node", () => {
    const builder = new Builder("root/pkg");
    const next = () => void 0;

    builder.visitNode("root/pkg", "1.0", "package", next);
    builder.visitNode("a", "1.0", "package", next);

    builder.visitLink("b", "a", "^1.0", "prod", next);

    const graph = builder.build();

    expect(graph.hasNode("b")).toBeFalsy();
  });
});
