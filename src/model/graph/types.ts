export type NodeType =
  | "language"
  | "library"
  | "extension"
  | "package"
  | "namespace";

export type NodeID = string;

export interface Node {
  readonly id: NodeID;
  readonly version: string;
  readonly type: NodeType;
  readonly label?: string;
  readonly tooltip?: string;
  linksTo: ReadonlyArray<Link>;
  linkedFrom: Set<NodeID>;
}

export type LinkType = "prod" | "dev";

export interface Link {
  readonly source: NodeID;
  readonly target: NodeID;
  readonly constraint: string;
  readonly type: LinkType;
  readonly label?: string;
}

export interface Visitor {
  visitNode(
    id: NodeID,
    version: string,
    type: NodeType,
    visitLinks: () => void
  ): void;
  visitLink(
    source: NodeID,
    target: NodeID,
    constraint: string,
    type: LinkType,
    visitTarget: () => void
  ): void;
}
