/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  cacheDirectory: ".cache/jest",
  testMatch: ["**/*.test.ts"],
  collectCoverageFrom: ["src/**/*.ts"],
  reporters: ["default", ["jest-junit", { outputDirectory: "reports" }]],
  coverageDirectory: "reports",
  coverageReporters: ["text", "text-summary", "cobertura"],
};
